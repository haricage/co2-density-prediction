# CO2 Density Prediction

This is a readme file for swdco2.jl. Code written by Hariharan Ramachandran.<br>

The documentation is available in the page --> https://haricage.gitlab.io/co2-density-prediction

**Usage**<br>
This [Julia](https://julialang.org/) code calculates the density of pure CO2 based on the [Span-Wagner Technical Equation Of State](https://link.springer.com/article/10.1023/A:1022390430888) (Multiparameter EOS Formulation). This code was used in [this](https://www.sciencedirect.com/science/article/pii/S1876610217316764) publication and [this](https://repositories.lib.utexas.edu/handle/2152/63653) dissertation.<br>

Run swdco2 in the runtime environment. In the Julia Repl window, type --> swdco2(Pressure in MPa, Temperature in K). The density output will be in kg/m3.<br>

Example --> swdco2(7,300)<br>
Output --> (0.0, 707.3213717078211)<br>
The first term in the output is the gas density and the second term is the liquid density.<br>


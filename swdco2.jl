#=
MIT License

Copyright (c) 2020 Hariharan Ramachandran

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
=#
function swdco2(pin::Number,tin::Number)
#--------------------------------------------------------------------------
# General comments
# this equations are from technical EOS refer Span-Wagner eos
#--------------------------------------------------------------------------
    tol::Float64 = 10.0^-7;        # tolerence for general convergence
    dtol::Float64 = 0.001;       # tolerence for density difference
#--------------------------------------------------------------------------
    # Parameters for the general calculations
    # Refer the span wagner reduced paper and full version
    #molwt = 44.0098;            # molecular weight in g/mol
    tc::Float64 = 304.1282;              # critical temperature in K
    rhoc::Float64 = 467.6;               # critical density in kg/m3
    rmol::Float64 = 8.314510;            # molar gas constant in j/mol/k
    r::Float64 = rmol/44.0098;             # specific gas constant in j/g/K
    pc::Float64 = 7.3773;                # critical pressure in MPa
    rhoceos::Float64 = 446.62;           # critical density in kg/m3
    # Calculated values from this specific optimized EOS. Refer SW reduced form paper
    #tc = 304.307; #pc = 7.399; #omega = .225
#--------------------------------------------------------------------------
    # input
    p::Float64 = pin;
    t::Float64 = tin;
#--------------------------------------------------------------------------
    # Function to solve for density
    # Newton-Raphson algorithm over del. Will generate two different results based on initial guesses.
    function densol(p,r,t,rhoc,tau,dguess)
        count2::Int64 = 0;
        maxval::Int64 = 100;
        dold = dguess;
        dnew::Float64 = 0.1;
        fold::Float64 = 0.0;fdash::Float64 = 0.0;
        fold = (1000.0*p/rhoc/dold/r/t)-1.0-dold*dalfrdel(tau,dold);
        fdash = -(1000.0*p/rhoc/dold^2.0/r/t)-dalfrdel(tau,dold)-dold*d2alfrdel(tau,dold);
        for i in 1:maxval
            if dold < 0.0 || dold > 3.0
                if dguess < 1.0
                    dnew = 0.4;
                    count2 = count2 +1;
                else
                    dnew = 1.3;
                    count2 = count2 +1;
                end
            else
                dnew = dold-fold/fdash;
            end
            fnew = (1000.0*p/rhoc/dnew/r/t)-1.0-dnew*dalfrdel(tau,dnew);
            fdash = -(1000.0*p/rhoc/dnew^2.0/r/t)-dalfrdel(tau,dnew)-dnew*d2alfrdel(tau,dnew);
            if abs(fnew) < tol
                break
            else
                dold = dnew;
                fold = fnew;
            end
            if i == maxval || count2 == 2
                dnew = 10.0;
                break
            end
        end
        del = dnew;
        fugacity = exp(alfr(tau,del)+del*dalfrdel(tau,del)-log(1.0+del*dalfrdel(tau,del)));
        den = del*rhoc;
        return den, fugacity;
    end
#--------------------------------------------------------------------------
#--------------------------------------------------------------------------
# All the subsequent functions are calculated based on equations from span
# wagner book/ papers (reduced form and full form)
    # Function to calculate alpha residual
    function alfr(tau,del)
        # 12 constants #n1 = 0.89875108; #n2 = -2.1281985; #n3 = -0.068190320;
        #n4 = 0.076355306; #n5 = 0.00022053253; #n6 = 0.41541823; #n7 = 0.71335657;
        #n8 = 0.00030354234; #n9 = -0.36643143; #n10 = -0.0014407781;
        #n11 = -0.089166707; #n12 = -0.023699887;
        # estimation of phir
        alphar1 = 0.89875108*del*(tau^.25)-2.1281985*del*(tau^1.25)-0.068190320*del*(tau^1.5)+0.076355306*(del^3.0)*(tau^.25);
        alphar2 = 0.00022053253*(del^7.0)*(tau^.875)+0.41541823*del*(tau^2.375)*exp(-del)+0.71335657*(del^2.0)*(tau^2.0)*exp(-del);
        alphar3 = 0.00030354234*(del^5.0)*(tau^2.125)*exp(-del)-0.36643143*del*(tau^3.5)*exp(-del^2.0)-0.0014407781*del*(tau^6.5)*exp(-del^2.0);
        alphar4 = -0.089166707*(del^4.0)*(tau^4.75)*exp(-del^2.0)-0.023699887*(del^2.0)*(tau^12.5)*exp(-del^3.0);
        alphar = alphar1+alphar2+alphar3+alphar4;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alpha residual w.r.t delta
    function dalfrdel(tau,del)
        # 12 constants
        n1 = 0.89875108;
        n2 = -2.1281985;
        n3 = -0.068190320;
        n4 = 0.076355306;
        n5 = 0.00022053253;
        n6 = 0.41541823;
        n7 = 0.71335657;
        n8 = 0.00030354234;
        n9 = -0.36643143;
        n10 = -0.0014407781;
        n11 = -0.089166707;
        n12 = -0.023699887;
        # estimation of dalfdel
        alphar1 = n1*(tau^.25)+n2*(tau^1.25)+n3*(tau^1.5)+3.0*n4*(del^2.0)*(tau^.25)+7.0*n5*(del^6.0)*(tau^.875);
        alphar2 = n6*(1.0-del)*(tau^2.375)*exp(-del)+n7*(2.0*del-del^2.0)*(tau^2.0)*exp(-del)+n8*(5.0*del^4.0-del^5.0)*(tau^2.125)*exp(-del)+n9*(1.0-2.0*del^2.0)*(tau^3.5)*exp(-del^2.0)+n10*(1.0-2.0*del^2.0)*(tau^6.5)*exp(-del^2.0)+n11*(4.0*del^3.0-2.0*del^5.0)*(tau^4.75)*exp(-del^2.0)+n12*(2.0*del-3.0*del^4.0)*(tau^12.5)*exp(-del^3.0);
        alphar = alphar1+alphar2;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate  double differential of alpha residual w.r.t delta
    function d2alfrdel(tau,del)
        # 12 constants
        n1 = 0.89875108;
        n2 = -2.1281985;
        n3 = -0.068190320;
        n4 = 0.076355306;
        n5 = 0.00022053253;
        n6 = 0.41541823;
        n7 = 0.71335657;
        n8 = 0.00030354234;
        n9 = -0.36643143;
        n10 = -0.0014407781;
        n11 = -0.089166707;
        n12 = -0.023699887;
        # estimation of dalfdel
        alphar1 = 2.0*3.0*n4*(del)*(tau^.25)+6.0*7.0*n5*(del^5.0)*(tau^.875);
        alphar2 = n6*(del-2.0)*(tau^2.375)*exp(-del)+n7*(del^2.0-4.0*del+2.0)*(tau^2.0)*exp(-del)+n8*(del^5.0-10.0*del^4.0+20.0*del^3.0)*(tau^2.125)*exp(-del)+n9*(4.0*del^3.0-6.0*del)*(tau^3.5)*exp(-del^2.0)+n10*(4.0*del^3.0-6.0*del)*(tau^6.5)*exp(-del^2.0)+n11*(4.0*del^6.0-18.0*del^4.0+12.0*del^2.0)*(tau^4.75)*exp(-del^2.0)+n12*(9.0*del^6.0-18.0*del^3.0+2.0)*(tau^12.5)*exp(-del^3.0);
        alphar = alphar1+alphar2;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alpha residual w.r.t tau
    function dalfrtau(tau,del)
        # 12 constants
        n1 = 0.89875108;
        n2 = -2.1281985;
        n3 = -0.068190320;
        n4 = 0.076355306;
        n5 = 0.00022053253;
        n6 = 0.41541823;
        n7 = 0.71335657;
        n8 = 0.00030354234;
        n9 = -0.36643143;
        n10 = -0.0014407781;
        n11 = -0.089166707;
        n12 = -0.023699887;
        # estimation of phir
        alphar1 = 0.25*n1*del*(tau^-0.75)+1.25*n2*del*(tau^0.25)+1.5*n3*del*(tau^0.5)+0.25*n4*(del^3.0)*(tau^-0.75)+.875*n5*(del^7.0)*(tau^-0.125);
        alphar2 = 2.375*n6*del*(tau^1.375)*exp(-del)+2.0*n7*(del^2.0)*(tau^1.0)*exp(-del)+2.125*n8*(del^5.0)*(tau^1.125)*exp(-del)+3.5*n9*del*(tau^2.5)*exp(-del^2.0)+6.5*n10*del*(tau^5.5)*exp(-del^2.0)+4.75*n11*(del^4.0)*(tau^3.75)*exp(-del^2.0)+12.5*n12*(del^2.0)*(tau^11.5)*exp(-del^3.0);
        alphar = alphar1+alphar2;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate double differential of alpha residual w.r.t tau
    function d2alfrtau(tau,del)
        # 12 constants
        n1 = 0.89875108;
        n2 = -2.1281985;
        n3 = -0.068190320;
        n4 = 0.076355306;
        n5 = 0.00022053253;
        n6 = 0.41541823;
        n7 = 0.71335657;
        n8 = 0.00030354234;
        n9 = -0.36643143;
        n10 = -0.0014407781;
        n11 = -0.089166707;
        n12 = -0.023699887;
        # estimation of phir
        alphar1 = -0.75*0.25*n1*del*(tau^-1.75)+0.25*1.25*n2*del*(tau^-0.75)+0.5*1.5*n3*del*(tau^-0.5)-0.75*0.25*n4*(del^3.0)*(tau^-1.75)-0.125*0.875*n5*(del^7.0)*(tau^-1.125);
        alphar2 = 1.375*2.375*n6*del*(tau^0.375)*exp(-del)+1.0*2.0*n7*(del^2.0)*(tau^0.0)*exp(-del)+1.125*2.125*n8*(del^5.0)*(tau^0.125)*exp(-del)+2.5*3.5*n9*del*(tau^1.5)*exp(-del^2.0)+5.5*6.5*n10*del*(tau^4.5)*exp(-del^2.0)+3.75*4.75*n11*(del^4.0)*(tau^2.75)*exp(-del^2.0)+11.5*12.5*n12*(del^2.0)*(tau^10.5)*exp(-del^3.0);
        alphar = alphar1+alphar2;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate double differential of alpha residual w.r.t tau and delta
    function d2alfrdeltau(tau,del)
        # 12 constants
        n1 = 0.89875108;
        n2 = -2.1281985;
        n3 = -0.068190320;
        n4 = 0.076355306;
        n5 = 0.00022053253;
        n6 = 0.41541823;
        n7 = 0.71335657;
        n8 = 0.00030354234;
        n9 = -0.36643143;
        n10 = -0.0014407781;
        n11 = -0.089166707;
        n12 = -0.023699887;
        # estimation of dalfdel
        alphar1 = 0.25*n1*(tau^-0.75)+1.25*n2*(tau^0.25)+1.5*n3*(tau^0.5)+0.25*3.0*n4*(del^2.0)*(tau^-0.75)+0.875*7.0*n5*(del^6.0)*(tau^-0.125);
        alphar2 = 2.375*n6*(1.0-del)*(tau^1.375)*exp(-del)+2.0*n7*(2.0*del-del^2.0)*(tau^1.0)*exp(-del)+2.125*n8*(5.0*del^4.0-del^5.0)*(tau^1.125)*exp(-del)+3.5*n9*(1.0-2.0*del^2.0)*(tau^2.5)*exp(-del^2.0)+6.5*n10*(1.0-2.0*del^2.0)*(tau^5.5)*exp(-del^2.0)+4.75*n11*(4.0*del^3.0-2.0*del^5.0)*(tau^3.75)*exp(-del^2.0)+12.5*n12*(2.0*del-3.0*del^4.0)*(tau^11.5)*exp(-del^3.0);
        alphar = alphar1+alphar2;
        return alphar;
    end
#--------------------------------------------------------------------------
    # Function to calculate standard state alphao
    function alfo(tau,del)
        a1 = 8.37304456;
        a2 = -3.70454304;
        a3 = 2.5;
        a4 = 1.99427042;
        a5 = 0.62105248;
        a6 = 0.41195293;
        a7 = 1.04028922;
        a8 = 0.08327678;
        t4 = 3.15163;
        t5 = 6.1119;
        t6 = 6.77708;
        t7 = 11.32384;
        t8 = 27.08792;
        alphao1 = log(del)+a1+a2*tau+a3*log(tau);
        alphao2 = a4*log(1.0-exp(-tau*t4))+a5*log(1.0-exp(-tau*t5))+a6*log(1.0-exp(-tau*t6))+a7*log(1.0-exp(-tau*t7))+a8*log(1.0-exp(-tau*t8));
        alphao = alphao1+alphao2;
        return alphao;
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alphao w.r.t tau
    function dalfotau(tau,del)
        a1 = 8.37304456;
        a2 = -3.70454304;
        a3 = 2.5;
        a4 = 1.99427042;
        a5 = 0.62105248;
        a6 = 0.41195293;
        a7 = 1.04028922;
        a8 = 0.08327678;
        t4 = 3.15163;
        t5 = 6.1119;
        t6 = 6.77708;
        t7 = 11.32384;
        t8 = 27.08792;
        alphao1 = a2+a3/tau;
        alphao2 = a4*t4/(exp(t4*tau)-1.0)+a5*t5/(exp(t5*tau)-1.0)+a6*t6/(exp(t6*tau)-1.0)+a7*t7/(exp(t7*tau)-1.0)+a8*t8/(exp(t8*tau)-1.0);
        alphao = alphao1+alphao2;
        return alphao;
    end
#--------------------------------------------------------------------------
    # Function to calculate double differential of alphao w.r.t tau
    function d2alfotau(tau,del)
        a1 = 8.37304456;
        a2 = -3.70454304;
        a3 = 2.5;
        a4 = 1.99427042;
        a5 = 0.62105248;
        a6 = 0.41195293;
        a7 = 1.04028922;
        a8 = 0.08327678;
        t4 = 3.15163;
        t5 = 6.1119;
        t6 = 6.77708;
        t7 = 11.32384;
        t8 = 27.08792;
        alphao1 = -2.5/tau^2.0;
        alphao2 = a4*t4^2.0*exp(t4*tau)/(exp(t4*tau)-1.0)^2.0+a5*t5^2.0*exp(t5*tau)/(exp(t5*tau)-1.0)^2.0+a6*t6^2.0*exp(t6*tau)/(exp(t6*tau)-1.0)^2.0+a7*t7^2.0*exp(t7*tau)/(exp(t7*tau)-1.0)^2.0+a8*t8^2.0*exp(t8*tau)/(exp(t8*tau)-1.0)^2.0;
        alphao = alphao1-alphao2;
        return alphao;
    end
#--------------------------------------------------------------------------
    # Function to calculate differential of alphao w.r.t delta
    function dalfodel(tau,del)
        alphao = 1.0/del;
        return alphao;
    end
#--------------------------------------------------------------------------
    # Function to calculate double differential of alphao w.r.t delta
    function d2alfodel(tau,del)
        alphao = -1.0/del^2.0;
        return alphao;
    end
#--------------------------------------------------------------------------
# Phase equilibrium calculation
# This will solve for density and check the fugacity to find out the most stable root/roots
tau = tc/t;
# Using denslover function to estimate density and fugacity.
#deng::Float64 = 0.0; fugg::Float64 = 0.0;
#denl::Float64 = 0.0; fugl::Float64 = 0.0;
(deng, fugg) = densol(p,r,t,rhoc,tau,0.001);
(denl, fugl) = densol(p,r,t,rhoc,tau,2.0);
# Check if the densities are the same
errd::Float64 = abs(deng-denl)::Float64;
if errd < dtol
    if deng <=rhoceos
        fugl = 10000000.0;
    else
        fugg = 10000000.0;
    end
end
# Fugacity check to identify stable roots
errf::Float64 = abs(fugg-fugl);
if errf < tol
    deng = deng;
    denl = denl;
elseif fugg < fugl
    deng = deng;
    denl = 0.0;
else
    deng = 0.0;
    denl = denl;
end
den = (deng,denl);      # density is in kg/m3
#println("density = $den")
return den;
#------------------------------------------------------------------------------
end
